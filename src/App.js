// import logo from './logo.svg';
import "./App.css";
import Home from "./components/Home/home";
import { useEffect, useContext } from "react";
import useHttp from "./hooks/use-http";
import TopicContext from "./components/Store/topic-context";
import NoteContext from "./components/Store/note-context";

function App() {
  const topicCtx = useContext(TopicContext);
  const noteCtx = useContext(NoteContext)
  const {  setTopics } = topicCtx;
  const { setNotes } = noteCtx
  const {  sendRequest: fetchTopics } = useHttp();
  const {  sendRequest: fetchNotes } = useHttp();

  useEffect(() => {

    // -----------------  Topics Fetching ------------------------
    const topicLoad = (topics) => {
      const loadedTopics = [];

      for (const topicKey in topics) {
        loadedTopics.push({
          id: topicKey,
          value: topics[topicKey].value,
          subject: topics[topicKey].subject,
        });
      }
      setTopics(loadedTopics);
    };

    fetchTopics(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/topics.json",
      },
      topicLoad
    );

    // -----------------  Notes Fetching ------------------------
    const noteLoad = (notes)=>{
      const loadedNotes = [];
      
      for( const noteKey in notes){
        loadedNotes.push({
          id:noteKey,
          value:notes[noteKey].value,
          topic:notes[noteKey].topic,
        });
      }
      // console.log(loadedNotes)
      setNotes(loadedNotes);
    }

    fetchNotes(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/notes.json",
      },
      noteLoad
    );

  }, []);

  return (
    <div>
      <Home />
    </div>
  );
}

export default App;
