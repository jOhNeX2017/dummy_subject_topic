import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { TopicContextProvider } from "./components/Store/topic-context";
import { NoteContextProvider } from "./components/Store/note-context";
import { ModalContextProvider } from "./components/Store/modal-context";

ReactDOM.render(
  <React.StrictMode>
    <ModalContextProvider>
      <TopicContextProvider>
        <NoteContextProvider>
          <App />
        </NoteContextProvider>
      </TopicContextProvider>
    </ModalContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
