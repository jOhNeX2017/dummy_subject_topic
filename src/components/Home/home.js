import React from "react";
import SubjectComponent from "../Subjects/subjects";
import classes from "./home.module.css";

const Home = () => {
  return (
    <React.Fragment>
     
        <header className={classes["main-header"]}>
          <h1>Dummy Subject Topic and Note</h1>
        </header>
        <div className={classes.container}>
          <SubjectComponent />
        </div>
      
    </React.Fragment>
  );
};

export default Home;
