import React, { useContext } from "react";
import styles from "./Modal.module.css";
import ReactDOM from "react-dom";
import ModalContext from "../../Store/modal-context";

const BackDrop = (props) => {
  const ctx = useContext(ModalContext);
  return <div className={styles.backdrop} onClick={ctx.toggleModal}></div>;
};

const ModalOverlay = (props) => {
  return <div className={styles.modal}>{props.children}</div>;
};
const Modal = (props) => {
  return (
    <React.Fragment>
      {ReactDOM.createPortal(
        <BackDrop />,
        document.getElementById("backdrop-root")
      )}
      {ReactDOM.createPortal(
        <ModalOverlay>{props.children}</ModalOverlay>,
        document.getElementById("modaloverlay-root")
      )}
    </React.Fragment>
  );
};

export default Modal;
