import React, { useState, useEffect } from "react";
import TreeItem from "@material-ui/lab/TreeItem";

const NotesComponent = (props) => {
  const [noteCollection, setNoteCollection] = useState([]);
  const { topic, notes } = props;

  useEffect(() => {
    const collect = [];

    for (var i in notes) {
      if (notes[i].topic === topic) {
        collect.push(notes[i]);
      }
    }

    setNoteCollection(collect);
  }, [topic, notes]);

  return (
    <React.Fragment>
      {noteCollection.length > 0 &&
        noteCollection.map((note) => {
          return (
            // <p>{note.value}</p>
            <TreeItem nodeId={note.id} key={note.id} label={note.value} />
          );
        })}
      {noteCollection.length === 0 && (
        <TreeItem
          nodeId="note-length"
          key="note-length"
          label="Please add note first"
        />
      )}
    </React.Fragment>
  );
};

export default NotesComponent;
