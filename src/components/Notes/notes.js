import { useEffect, useState, Fragment, useContext } from "react";
import NoteView from './noteView';
import NoteContext from "../Store/note-context";


const NotesComponent = (props)=> {
    const noteCtx = useContext(NoteContext);
    const { notes:noteData } = noteCtx;
    const [initialNotes, setinitialNotes] = useState(noteData);

    useEffect(()=>{
        setinitialNotes(noteData);
    },[noteData])

    // console.log(noteData)
    let notes;
    // console.log(initialNotes)

    if (initialNotes && initialNotes.length >0 ){
        notes = <NoteView notes={initialNotes} topic={props.topic} />
    }

    return (
        <Fragment>
            {notes}
        </Fragment>
    );
}

export default NotesComponent;

