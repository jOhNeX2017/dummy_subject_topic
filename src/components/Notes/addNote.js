import useHttp from "../../hooks/use-http";
import NoteContext from "../Store/note-context";
import NoteForm from "./noteForm";
import { useContext } from "react";

const AddNote = (props) => {
  const { error, sendRequest: sendAddNote } = useHttp();
  const noteCtx = useContext(NoteContext);

  const createNote = (noteTopic, noteText, noteObj) => {
    const generateId = noteObj.name;
    const createNewNote = {
      id: generateId,
      value: noteText,
      topic: noteTopic,
    };
    noteCtx.setNotes(createNewNote);
  };
  const enterNoteHanlder = (noteTopic, noteText) => {
    sendAddNote(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/notes.json",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: {
          value: noteText,
          topic: noteTopic,
        },
      },
      createNote.bind(null, noteTopic, noteText)
    );
  };
  return <NoteForm addNoteHandler={enterNoteHanlder} topics={props.topics} notes={noteCtx.notes} />;
};

export default AddNote;

