import React, { useRef } from "react";
import classes from "./note.module.css";

const NoteForm = (props) => {
  const inputRef = useRef();
  const selectRef = useRef();
  const topicsSelectList = props.topics.map((topic) => (
    <option value={topic.value}>{topic.value}</option>
  ));

  const submitHandler = (event) => {
    event.preventDefault();
    const inputValue = inputRef.current.value;
    const selectValue = selectRef.current.value;
    let noteValid;
    
    if(selectValue.trim().length === 0 ){
      alert('Please Select the topic first')
      return;
    }

    if (inputValue.trim().length > 0) {
      noteValid = props.notes.filter(
        (note_val) =>
          note_val.topic === selectValue && note_val.value === inputValue
      );
      if (noteValid && noteValid.length > 0) {
        console.clear();
        console.log(inputValue + " already present for " + selectValue);

        alert(inputValue + "  already exist in " + selectValue);
        inputRef.current.value = "";
        return;
      } else {
        props.addNoteHandler(selectValue, inputValue);
        alert("Note Added Succesfully");
      }
    } else {
      alert("Please Input the valid value");
      inputRef.current.value = "";
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="control-group">
        <div className={classes.control}>
          <select ref={selectRef}>
            <option selected value="">Please select the topic</option>
            {topicsSelectList}
          </select>
        </div>
        <div className={classes.control}>
          <input
            type="text"
            required
            ref={inputRef}
            placeholder="Please enter Note Name"
          />
        </div>
        <button type="submit">Submit</button>
      </div>
    </form>
  );
};

export default NoteForm;
