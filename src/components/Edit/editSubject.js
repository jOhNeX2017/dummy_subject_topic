import React, { useRef } from "react";
import useHttp from "../../hooks/use-http";

const EditSubject = (props) => {
  const inputRef = useRef();
  const selectRef = useRef();
  const {sendRequest:sendUpdateSubject} = useHttp();

  const subjectSelect = props.subjects.map((subject) => {
    return <option value={subject.value}>{subject.value}</option>;
  });

  const updateSubject = (subjectId,subjectText, subjectObj) => {
    const genrateeId = subjectId.name;
    const updatedSubject = { id: genrateeId, value: subjectText };
    // console.log(createSubject)
    props.subjectHandler(updatedSubject);
  };

  const enterSubjectHandler = (subjectId, subjectText) => {
    sendUpdateSubject(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/subjects.json",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body:{
            id:subjectId,
            value: subjectText,
        },
      },
      updateSubject.bind(null,subjectId, subjectText)
    );
  }
  const submitHandler = (event) => {
    event.preventDefault();

    const inputValue = inputRef.current.value;
    const selectValue = selectRef.current.value;

    if(inputValue.trim().length >0){
        const selectedSubject = props.subjects.filter(
            (sub) => sub.value === selectValue
          );
          console.clear()
        //   console.log(selectedSubject[0].id,inputValue)
        //   props.subjects.push({id:selectedSubject[0].id,value:inputValue})
        //   console.log(props.subjects)
        //   enterSubjectHandler(props.subjects)
          enterSubjectHandler(selectedSubject[0].id,inputValue)
    }
    else{
        alert('Please input valid Value')
    }
  };

  return (
    <div>
      <form onSubmit={submitHandler}>
        <select ref={selectRef}>{subjectSelect}</select>
        <label for="value"> New Value </label>
        <input
          type="text"
          name="value"
          value={props.value}
          ref={inputRef}
          required
        />
        <button type="submit">Change</button>
        <button type="button">Cancel</button>
      </form>
    </div>
  );
};

export default EditSubject;
