import React,{ useContext } from 'react';
import Modal from '../UI/Modal/Modal';
import ModalContext from "../Store/modal-context";

const EditComponent = (props) => {
    const modalCtx = useContext(ModalContext);
    return (
        <Modal>
            <form>
                <label for="id"> id </label> 
                <input type="text" name="id" value={props.id} disabled required />
                <br/>
                <label for="value"> Value </label> 
                <input type="text" name="value" value={props.value}  required/>
                <button type="submit">Change</button>
                <button type="button">Cancel</button>
            </form>
        </Modal>
    )
}

export default EditComponent;