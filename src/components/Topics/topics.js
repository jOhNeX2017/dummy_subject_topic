// import useHttp from "../../hooks/use-http";
import { useEffect, useState, Fragment, useContext } from "react";
import TopicView from "./topicView";
import TopicContext from "../Store/topic-context";

const TopicsComponent = (props) => {
  const topicCtx = useContext(TopicContext);
  const { topics: topicData } = topicCtx;
  const [intialTopics, setInitialTopics] = useState(topicData);

  useEffect(() => {
    setInitialTopics(topicData);
  }, [topicData]);

  let topics;

  if(intialTopics && intialTopics.length>0){
    topics = <TopicView topics={intialTopics} subject={props.subject} />;
  }
 

  return <Fragment>{topics}</Fragment>;
};

export default TopicsComponent