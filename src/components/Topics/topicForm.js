import React, { useRef } from "react";
import classes from "./topic.module.css";

const TopicForm = (props) => {
  const inputRef = useRef();
  const selectRef = useRef();
  const subjectSelectList = props.subjects.map((subject) => (
    <option value={subject.value}>{subject.value}</option>
  ));

  const submitHandler = (event) => {
    event.preventDefault();
    const inputValue = inputRef.current.value;
    const selectValue = selectRef.current.value;

    let topicValid;

    if(selectValue.trim().length === 0 ){
      alert('Please Select the topic first')
      return;
    }

    if (inputValue.trim().length > 0) {
      topicValid = props.topics.filter(
        (topic_val) =>
          topic_val.subject === selectValue && topic_val.value === inputValue
      );
      if (topicValid && topicValid.length > 0) {
        console.clear();
        console.log(inputValue + " already present for " + selectValue);

        alert(inputValue + "  already exist in " + selectValue);
        inputRef.current.value = "";
        return;
      } else {
        props.addTopicHandler(selectValue, inputValue);
        alert("Topic Added Succesfully");
      }
    } else {
      alert("Please Input the valid value");
      inputRef.current.value = "";
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="control-group">
        <div className={classes.control}>
          <select ref={selectRef} name="subject">
          <option selected value="">Please select the subject</option>
            {subjectSelectList}
            </select>
        </div>
        <div className={classes.control}>
          <input
            type="text"
            required
            ref={inputRef}
            placeholder="Please enter topic name"
          />
        </div>
        <button type="submit">Submit</button>
      </div>
    </form>
  );
};

export default TopicForm;
