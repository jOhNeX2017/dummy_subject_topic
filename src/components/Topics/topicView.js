import React, { useState, useEffect } from "react";
import NotesComponent from "../Notes/notes";
import TreeItem from "@material-ui/lab/TreeItem";

const TopicView = (props) => {
  const [topicsCollection, setTopicsCollection] = useState([]);
  // console.log(props.subject)

  useEffect(() => {
    const collect = [];
    for (var i in props.topics) {
      // console.log(props.subject[i].subject);
      if (props.topics[i].subject === props.subject) {
        collect.push(props.topics[i]);
      }
    }
    // console.log(props.subject);
    setTopicsCollection(collect);
  }, [props.topics, props.subject]);

  return (
    <>
      {props.topics.length === 0 && (
        <TreeItem
          nodeId="topic-length"
          key="topic-length"
          label="Please add topic first"
        />
      )}
      {topicsCollection.map((topic) => (
        <TreeItem nodeId={topic.id} key={topic.id} label={topic.value}>
          {/* <p>{val.value}</p> */}
          <NotesComponent topic={topic.value} />
        </TreeItem>
      ))}
      
    </>
  );
};

export default TopicView;
