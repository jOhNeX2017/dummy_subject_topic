import useHttp from "../../hooks/use-http";
import TopicContext from "../Store/topic-context";
import TopicForm from "./topicForm";
import { useContext } from "react";

const AddTopics = (props) => {
  const { error, sendRequest: sendAddTopic } = useHttp();
  const topicCtx = useContext(TopicContext);

  const createTopics = (topicSubject, topictext, topicObj) => {
    const generateId = topicObj.name;
    const createTopic = {
      id: generateId,
      value: topictext,
      subject: topicSubject,
    };
    topicCtx.setTopics(createTopic);
    // props.topicHandler(createTopic);
  };

  const enterTopicHandler = (topicSubject, topicText) => {
    sendAddTopic(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/topics.json",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: {
          value: topicText,
          subject: topicSubject,
        },
      },
      createTopics.bind(null, topicSubject, topicText)
    );
  };
  return (
    <TopicForm addTopicHandler={enterTopicHandler} subjects={props.subjects} topics={topicCtx.topics}/>
  );
};

export default AddTopics;
