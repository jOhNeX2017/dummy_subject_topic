// import useHttp from "../../hooks/use-http";
import React,{useState} from 'react';

const TopicContext = React.createContext({
    topics:[],
    setTopics:(data)=>{}
});

export const TopicContextProvider =(props) => {

    const [topic,setTopic] = useState([]);

    const topicHandler = (data) => {
        // console.log(data)
        setTopic((prevTopic) => prevTopic.concat(data))
    };

    return(<TopicContext.Provider value={{
        topics:topic,
        setTopics:topicHandler,
    }}>
        {props.children}
    </TopicContext.Provider>)    

}

export default TopicContext;