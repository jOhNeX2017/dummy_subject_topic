import React, {useState} from "react";

const NoteContext = React.createContext({
    notes:[],
    setNotes : (data) => {},
});

export const NoteContextProvider = (props) => {
    const [note,setNote] = useState([]);

    const noteHandler =  (data) =>{
        // console.log(data)
        setNote((prevNote) => prevNote.concat(data))
    }

    return (<NoteContext.Provider value={{
        notes:note,
        setNotes:noteHandler,
    }}>
        {props.children}
    </NoteContext.Provider>)
};

export default NoteContext;