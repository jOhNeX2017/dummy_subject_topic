import React, { useState} from 'react';

const ModalContext = React.createContext({
    isOpen:null,
    toggleModal:()=>{}
});

export default ModalContext;

export const ModalContextProvider = (props) =>{

    const [modalVisible,setModalVisible] = useState(false);

    const toggleModal = () =>{
        setModalVisible(!modalVisible);
    }

    return(
        <ModalContext.Provider value={{
            isOpen:modalVisible,
            toggleModal:toggleModal,
        }}>
            {props.children}
        </ModalContext.Provider>

    )
};
