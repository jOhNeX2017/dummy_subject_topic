import React, { useRef } from "react";
import classes from "./subject.module.css";

const SubjectForm = (props) => {
  const inputRef = useRef();
  const submitHandler = (event) => {
    event.preventDefault();

    const inputValue = inputRef.current.value;

    let subjectValid;

    if (inputValue.trim().length > 0) {
      subjectValid = props.subjects.filter((sub) => sub.value === inputValue);
      if (subjectValid && subjectValid.length > 0) {
        console.clear();
        console.log("Subject Already present");

        alert("Subject Already Exist");
        inputRef.current.value = "";
        return;
      } else {
        props.enterSubjectHandler(inputValue);
        alert("Subject Added Succesfully");
      }
    } else {
      alert("Please enter the valide value");
      inputRef.current.value = "";
    }
  };

  return (
    <>
      <form onSubmit={submitHandler}>
      <div className="control-group">
        <div className={classes.control}>
          <label for="subject-name">Subject Name</label>
          <input type="text" required ref={inputRef} name="subject-name" placeholder="Please input subject name"/>
        </div>
        <button type="submit">Submit</button>
        </div>
      </form>

    </>
  );
};

export default SubjectForm;
