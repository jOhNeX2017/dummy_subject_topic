import useHttp from "../../hooks/use-http";
import SubjectForm from "./subjectForm";

const AddSubject = (props) => {
  const { isLoading, error, sendRequest: sendAddSubject } = useHttp();

  const createSubject = (subjectText, subjectObj) => {
    const genrateeId = subjectObj.name;
    const createSubject = { id: genrateeId, value: subjectText };
    // console.log(createSubject)
    props.subjectHandler(createSubject);
  };

  const enterSubjectHandler = (subjectText) => {
    sendAddSubject(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/subjects.json",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: {
          value: subjectText,
        },
      },
      createSubject.bind(null, subjectText)
    );
  };

  return (
    <>
      <SubjectForm enterSubjectHandler={enterSubjectHandler} subjects={props.subjects} />
      {/* {error && <p>{error}</p>} */}
    </>
  );
};

export default AddSubject;
