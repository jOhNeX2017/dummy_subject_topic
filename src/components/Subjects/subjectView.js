import { useEffect, useState,useContext } from "react";
import TopicsComponent from "../Topics/topics";
import { makeStyles } from "@material-ui/core/styles";
import TreeView from "@material-ui/lab/TreeView";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import TreeItem from "@material-ui/lab/TreeItem";
// import ModalContext from "../Store/modal-context";
// import Modal from '../UI/Modal/Modal'
// import EditComponent from '../Edit/edit';

const useStyles = makeStyles({
  root: {
    height: 216,
    flexGrow: 1,
    maxWidth: 400,
  },
});

const SubjectView = (props) => {
  const classes = useStyles();
  const [subjectCollection, setSubjectCollection] = useState([]);
  // const modalCtx = useContext(ModalContext);

  useEffect(() => {
    const collect = [];
    for (let i in props.subjects) {
      collect.push(props.subjects[i]);
    }
    setSubjectCollection(collect);
  }, [props.subjects]);

  return (
    <>
    
    <TreeView
      className={classes.root}
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      multiSelect
    >
      {subjectCollection.map((subject) => {
        // console.log(subject.id)
        return (
          <TreeItem nodeId={subject.id} key={subject.id} label={subject.value}>
            {/* <p>{subject.value}</p> */}
            {/* <button onClick={modalCtx.toggleModal}> Edit Subject</button>
            {modalCtx.isOpen && <EditComponent id={subject.id} value={subject.value} />} */}
            <TopicsComponent subject={subject.value} />
          </TreeItem>
        );
      })}
    </TreeView>
    
    </>
  );
};

export default SubjectView;
