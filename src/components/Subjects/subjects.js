import useHttp from "../../hooks/use-http";
import Loader from "../UI/Loader";
import { useEffect, useState, Fragment, useContext } from "react";
import SubjectView from "./subjectView";
import AddSubject from "./addSubject";
import AddTopics from "../Topics/addTopics";
import AddNote from "../Notes/addNote";
import TopicContext from "../Store/topic-context";
import classes from "./subject.module.css";
import Button from "../UI/Button/Button";
// import EditSubject from "../Edit/editSubject";

const SubjectComponent = () => {
  const [intitalSubjects, setInitialSubjects] = useState([]);
  const [formView, setFormView] = useState({
    subject: true,
    topic: false,
    note: false,
  });
  const { isLoading, error, sendRequest: fetchSubjects } = useHttp();
  const topicCtx = useContext(TopicContext);
  const { topics } = topicCtx;

  useEffect(() => {
    const subjectLoad = (subjects) => {
      const loadedSubjects = [];

      for (const subjectkey in subjects) {
        loadedSubjects.push({
          id: subjectkey,
          value: subjects[subjectkey].value,
        });
      }
      // console.log(loadedSubjects);

      setInitialSubjects(loadedSubjects);
    };
    fetchSubjects(
      {
        url: "https://dummy-subject-topic-notes-default-rtdb.firebaseio.com/subjects.json",
      },
      subjectLoad
    );
  }, [fetchSubjects]);

  let subject;

  if (isLoading) {
    subject = <Loader />;
  } else if (error) {
    subject = { error };
  } else {
    if (intitalSubjects && intitalSubjects.length > 0) {
      subject = <SubjectView subjects={intitalSubjects} />;
    } else {
      subject = <h2>No subject in database</h2>;
    }
  }
  //   console.log(subject)
  const subjectHandler = (subject) => {
    //   console.log(subject);
    setInitialSubjects((prevsubject) => prevsubject.concat(subject));
  };

  const subjectButtonHandler = () => {
    setFormView({ subject: true, topic: false, note: false });
  };
  const topicButtonHanlder = () => {
    setFormView({ subject: false, topic: true, note: false });
  };
  const noteButtonHandler = () => {
    setFormView({ subject: false, topic: false, note: true });
  };

  return (
    <Fragment>
      <center>
        <div className={classes.card}>
          {/* <EditSubject subjects={intitalSubjects} subjectHandler={subjectHandler} /> */}
          {subject}
        </div>
      </center>
      <br />
      <div className={classes.buttonGroup}>
        <Button onClick={subjectButtonHandler}> Add Subject </Button>
        <Button onClick={topicButtonHanlder}> Add Topics </Button>
        <Button onClick={noteButtonHandler}> Add Notes </Button>
      </div>
      <br />
      <div className={classes.card}>
        {formView.subject && (
          <AddSubject
            subjectHandler={subjectHandler}
            subjects={intitalSubjects}
          />
        )}
        {formView.topic && <AddTopics subjects={intitalSubjects} />}
        {formView.note && <AddNote topics={topics} />}
      </div>
    </Fragment>
  );
};

export default SubjectComponent;
